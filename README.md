#LineChartView

## 只需要传入`ArrayList<Integer>`类型的数据和最后一天的日期,它就会自动适配横轴和纵轴的间隔 ##
![image](imgs/img1.png)
![image](imgs/img2.png)
![image](imgs/img3.png)


	<name.fuhan.view.LineChartView
		android:id="@+id/lineChartView"
		android:layout_width="match_parent"
		android:layout_margin="20dp"
		android:layout_height="200dp" />


方法就一个

	/**
     * 数据应当以时间顺序排序
     * @param list 数据,注意,不支持负数
     * @param lastTime 最后一天的日期
     */
	public void setData(ArrayList<Integer> list, Date lastTime);